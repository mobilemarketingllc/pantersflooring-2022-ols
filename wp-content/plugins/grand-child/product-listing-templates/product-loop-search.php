<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row product-row">
    <?php 
           $col_class = 'col-md-3 col-sm-4 col-xs-6';
           $show_financing = get_option('sh_get_finance');

    if(get_option('salesbrand')!=''){
            $slide_brands = rtrim(get_option('salesbrand'), ",");
            $brandonsale = array_filter(explode(",",$slide_brands));
            $brandonsale = array_map('trim', $brandonsale);
        }
    ?>

    
<?php while ( have_posts() ): the_post(); ?>
<?php if(get_field('swatch_image_link')) {
    
    //collection field
    $collection = get_field('collection', $post->ID);
    $brand =  get_field('brand', $post->ID);
    ?>
    <div class="<?php echo $col_class; ?>">
    
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
  				<?php 
						$image = swatch_image_product_thumbnail(get_the_ID(),'222','222');							
							
					?>
            <img class="search-pro-img" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>      
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>		
			 
			 <?php if(( get_option('getcouponbtn') == 1 && get_option('salesbrand')=='') || (get_option('getcouponbtn') == 1 && in_array(sanitize_title($brand),$brandonsale))){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/promotion/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
            <?php if($show_financing == 1){?>
            <a href="<?php echo site_url(); ?>/flooring-financing/" target="_self" class="fl-button plp-getfinance-btn" role="button" >
                <span class="fl-button-text">FINANCING</span>
            </a><br />
           <?php } ?>
            <a class="link plp-view-product" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
        <?php } ?>
<?php endwhile; ?>
</div>
</div>